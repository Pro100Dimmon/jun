 <!-- 1. Add latest jQuery and fancyBox files -->

<script src="//code.jquery.com/jquery-3.3.1.min.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script> 
        
      <style>
        .thumb img {
            -webkit-filter: grayscale(0);
            filter: none;
            border-radius: 5px;
            background-color: #fff;
            border: 1px solid #ddd;
            padding: 5px;
        }

        .thumb img:hover {
            -webkit-filter: grayscale(1);
            filter: grayscale(1);
        }

        .thumb {
            padding: 5px;
        }
    </style>     
    
    <div class="container">
    <h1 class="h2 text-center">Фотоальбом наших животных</h1>
    <div class="row">
        <div class="col-md-3 col-sm-4 col-xs-6 thumb">
            <a data-fancybox="gallery" href="https://img4.goodfon.ru/original/1366x768/d/fb/kisa-kot-koshka-vzgliad-chiornyi.jpg">
                <img class="img-responsive" src="https://img4.goodfon.ru/original/1366x768/d/fb/kisa-kot-koshka-vzgliad-chiornyi.jpg" alt="">
            </a>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6 thumb">
            <a data-fancybox="gallery" href="https://img4.goodfon.ru/original/1366x768/d/12/kot-koshka-glaza.jpg">
                <img class="img-responsive" src="https://img4.goodfon.ru/original/1366x768/d/12/kot-koshka-glaza.jpg" alt="">
            </a>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6 thumb">
            <a data-fancybox="gallery" href="https://img4.goodfon.ru/original/1366x768/b/3f/kot-koshka-mordochka-nabliudenie-listia-boke.jpg">
                <img class="img-responsive" src="https://img4.goodfon.ru/original/1366x768/b/3f/kot-koshka-mordochka-nabliudenie-listia-boke.jpg" alt="">
            </a>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6 thumb">
            <a data-fancybox="gallery" href="https://img4.goodfon.ru/original/1366x768/3/40/kot-koshka-koteika-mordochka-vzgliad-2.jpg">
                <img class="img-responsive" src="https://img4.goodfon.ru/original/1366x768/3/40/kot-koshka-koteika-mordochka-vzgliad-2.jpg" alt="">
            </a>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6 thumb">
            <a data-fancybox="gallery" href="https://img4.goodfon.ru/original/1366x768/d/44/kot-mordochka-vzgliad-shotlandskii-visloukhii.jpg">
                <img class="img-responsive" src="https://img4.goodfon.ru/original/1366x768/d/44/kot-mordochka-vzgliad-shotlandskii-visloukhii.jpg" alt="">
            </a>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6 thumb">
            <a data-fancybox="gallery" href="https://img4.goodfon.ru/original/1366x768/1/67/koteika-lapka-mordochka-vzgliad.jpg">
                <img class="img-responsive" src="https://img4.goodfon.ru/original/1366x768/1/67/koteika-lapka-mordochka-vzgliad.jpg" alt="">
            </a>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6 thumb">
            <a data-fancybox="gallery" href="https://img4.goodfon.ru/original/1366x768/1/5c/regdoll-koshka-mordochka-golubye-glaza-vzgliad-portret-fon-f.jpg">
                <img class="img-responsive" src="https://img4.goodfon.ru/original/1366x768/1/5c/regdoll-koshka-mordochka-golubye-glaza-vzgliad-portret-fon-f.jpg" alt="">
            </a>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6 thumb">
            <a data-fancybox="gallery" href="https://img4.goodfon.ru/original/1366x768/6/b5/koshka-krasavitsa-vzgliad-listia-portret.jpg">
                <img class="img-responsive" src="https://img4.goodfon.ru/original/1366x768/6/b5/koshka-krasavitsa-vzgliad-listia-portret.jpg" alt="">
            </a>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6 thumb">
            <a data-fancybox="gallery" href="https://img4.goodfon.ru/original/1366x768/e/9f/kotionok-malysh-milyi.jpg">
                <img class="img-responsive" src="https://img4.goodfon.ru/original/1366x768/e/9f/kotionok-malysh-milyi.jpg" alt="">
            </a>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6 thumb">
            <a data-fancybox="gallery" href="https://img4.goodfon.ru/original/1366x768/7/77/koshka-ryzhaia-vzgliad.jpg">
                <img class="img-responsive" src="https://img4.goodfon.ru/original/1366x768/7/77/koshka-ryzhaia-vzgliad.jpg" alt="">
            </a>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6 thumb">
            <a data-fancybox="gallery" href="https://img4.goodfon.ru/original/1366x768/3/fd/kotik-koteika-fon.jpg">
                <img class="img-responsive" src="https://img4.goodfon.ru/original/1366x768/3/fd/kotik-koteika-fon.jpg" alt="">
            </a>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6 thumb">
            <a data-fancybox="gallery" href="https://img4.goodfon.ru/original/1366x768/e/f4/kot-koteika-mordochka-usy-portret.jpg">
                <img class="img-responsive" src="https://img4.goodfon.ru/original/1366x768/e/f4/kot-koteika-mordochka-usy-portret.jpg" alt="">
            </a>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6 thumb">
            <a data-fancybox="gallery" href="https://img4.goodfon.ru/original/1366x768/1/68/kot-kotionok-trava-leto.jpg">
                <img class="img-responsive" src="https://img4.goodfon.ru/original/1366x768/1/68/kot-kotionok-trava-leto.jpg" alt="">
            </a>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6 thumb">
            <a data-fancybox="gallery" href="https://img4.goodfon.ru/original/1366x768/f/23/cat-ginger-eyes-beautiful-sight.jpg">
                <img class="img-responsive" src="https://img4.goodfon.ru/original/1366x768/f/23/cat-ginger-eyes-beautiful-sight.jpg" alt="">
            </a>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6 thumb">
            <a data-fancybox="gallery" href="https://img4.goodfon.ru/original/1366x768/d/1a/kot-vzgliad-polosatyi-amerikanskii-korotkosherstnyi.jpg">
                <img class="img-responsive" src="https://img4.goodfon.ru/original/1366x768/d/1a/kot-vzgliad-polosatyi-amerikanskii-korotkosherstnyi.jpg" alt="">
            </a>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6 thumb">
            <a data-fancybox="gallery" href="https://img4.goodfon.ru/original/1366x768/5/e7/kot-glaza-usy-4.jpg">
                <img class="img-responsive" src="https://img4.goodfon.ru/original/1366x768/5/e7/kot-glaza-usy-4.jpg" alt="">
            </a>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6 thumb">
            <a data-fancybox="gallery" href="https://img4.goodfon.ru/original/1366x768/7/f0/kot-tsvety-floksy.jpg">
                <img class="img-responsive" src="https://img4.goodfon.ru/original/1366x768/7/f0/kot-tsvety-floksy.jpg" alt="">
            </a>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6 thumb">
            <a data-fancybox="gallery" href="https://img4.goodfon.ru/original/1366x768/b/8c/koshka-glaza-usy-fon-2.jpg">
                <img class="img-responsive" src="https://img4.goodfon.ru/original/1366x768/b/8c/koshka-glaza-usy-fon-2.jpg" alt="">
            </a>
        </div>
    </div>
</div>