<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=600">
        <title><?= $data['title'] ?></title>
      <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="/css/style.css">
        <link rel="stylesheet" href="/css/font-awesome.min.css">
    </head>
    <header>
        <nav class="navbar" role="navigation">
           <div class="container-fluid">       
                     <!-- Brand and toggle get grouped for better mobile display -->
                      <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                          <span class="sr-only">Toggle navigation</span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </button>
                          <a class="navbar-logo" href=""><img id="cat" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4EYDSIxp-mrYI_jowYLUiiHkfTlEuwTjk5Dmlcl2eubnfuhYO"></a>
            <a class="navbar-brand" href="#">Golden Berry</a>   
             </div>
                      
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               <ul class="nav navbar-nav navbar-right">
                     <li><a href="/">Главная</a></li>
                     <li><a href="/gallery">Галерея</a></li>

                     <?php foreach ($data['links'] as $link) { ?>
                     <li><a href="/page/<?= $link['id'] ?>"><?= $link['title'] ?></a></li>
                     <?php } ?>

                        
                     <?php if($data['user']){ ?>
                     <li><a href="/logout">Выйти</a></li>

                     <?php }else{ ?>

                     <li><a href="/login">Вход</a></li>
                     <li><a href="/registration">Регистрация</a></li>
                     <?php } ?>

                     <?php if($data['user'] && $data['user']['role']== 'ADMIN'){ ?>
                     <li><a href="/admin">Админ панель</a></li>
                         
                     <li class="submenu2">Привет , <?= $data['user'] ? $data['user']['login'] : 'Гость !' ?></li>
                     <?php } ?>
                
               </ul>
             </div>
          </div>
        </nav>
    </header>
    

    

      