<body>      
 <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="https://img4.goodfon.ru/original/1366x768/0/ce/black-cat-black-cat-kitten-black-kitten-kitty-chat-gatto-gat.jpg" alt="First slide">
          <div class="container">
            <div class="carousel-caption">
                <h1 id="title">Мечтаешь о домашнем питомце? Найди его здесь!</h1>
              <p>- Если вы мечтаете о домашнем питомце, здесь можно выбрать любимого и неповторимого члена семьи.</p>
              <p><a class="btn btn-lg btn-primary" href="/login/" role="button">Войдите на сайт</a></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img class="second-slide" src="https://img4.goodfon.ru/original/1366x768/4/b0/koshka-koteika-mordochka-vzgliad-fon.jpg" alt="Second slide">
          <div class="container">
            <div class="carousel-caption">
                <h1 id="title"> Не проходите мимо </h1>
              <p>- Если у вас есть свой приют, вы можете разместить информацию о нем и его питомцах на сайте.</p>
              <p><a class="btn btn-lg btn-primary" href="/page/1" role="button">О нас</a></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img class="third-slide" src="https://img4.goodfon.ru/original/1366x768/a/6d/cat-kot-koshka-1.jpg" alt="Third slide">
          <div class="container">
            <div class="carousel-caption">
                <h1 id="title">Главное верить !</h1>
              <p>- Не отчаивайтесь, ваш питомец обязательно найдется.</p>
              <p><a class="btn btn-lg btn-primary" href="/page/2" role="button">Перейти в галерею</a></p>
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div><!-- /.carousel -->
    
     <div id="works">
     <div class="container">
     <div class="row centered">
     <h4>Добро пожаловать!</h4>
     <br>
        <h3>Мы рады приветствовать Вас на сайте питомника "Golden Berry"!
   Наш питомник находится в городе Днепропетровск и занимается разведением кошек шотландской и британской короткошерстной. 
   Мы состоим в клубе "Алиса" системы WCF.
   Гамма окрасов с которой мы работаем, очень широка - от однотипных голубых и 
   лиловых до рисунчатых пятнистых и мраморных, а так же колор - поинтов.
   Основная цель нашего питомника - рождение здоровых и ласковых котят, 
   соответствующих стандарту породы.
   На этом сайте вы познакомитесь с производителями питомника, узнаете последние 
   новости, посмотрите на наших котят и возможно выберите себе плюшевого друга с чеширской улыбкой!</h3>

         <hr class="featurette-divider">
       	<div class="col-lg-12">
      	<div class="tilt">
          <a href="#"><img src="http://prodajakotyat.com/images/icon/bottom-cat-67698.png" alt=""></a>
        </div>
           </div>
          </div>
         </div>
        </div>
        <div class="container w">
        	<div class="row centered">
        		<br>
        		<div class="col-lg-4">
        			<i class="fa fa-heart"></i>
        			<h4>ВЫСТАВКА В КИЕВЕ!</h4>
        			<p>В Киеве 2-3 ноября состоялась 11 -12 международная лицензированная FIFe выставка кошек проводимая Фелинологическим Союзом Украины и КФПЦ Royal Feline. Принимавшая участие рыжая бенгалка Камилла получила BIV (лучшая в окрасе) и стала чемпионом. </p>
        		</div>
        		<div class="col-lg-4">
        			<i class="fa fa-laptop"></i>
        			<h4>ВЫСТАВКА В ЖИТОМИРЕ!</h4>
        			<p>В Житомире 14-15 декабря 2017 года состоялась выставка кошек по системе WCF, проводимая Житомирским областным фелинологическим центром "Вибрисс". Наш бенгальский кот Джокер стал BOS Junior (Лучшее животное выставки противоположного пола) первого дня и Best of Best Junior (Лучший из Лучших) второго дня. Два дня подряд наши бриташки - Ник, Хлое, бенгалы - Жасмин, Ельвира участвовали в номинации "Nom BIS". Британцы Цезарь, Зейн, Ник, бенгалка Жасмин стали чемпионами, Грациозная Селена получила титул Грандинтерчемпиона.</p>
        		</div>
        		<div class="col-lg-4">
        			<i class="fa fa-trophy"></i>
        			<h4>ВЫСТАВКА В ВИННИЦЕ!</h4>
        			<p>В Виннице 12-13 октября 2017 года состоялась I -II Международная лицензированная FIFe выставка проводимая Фелинологическим Союзом Украины и Винницким ОЦФЛ «Gold Cat». На этой выставке наша бенгалка Моника получила BIV (лучшая в окрасе) и стала чемпионкой.</p>
        		</div>
        	</div>
        	<br><br>
        </div>
       
     <!-- START THE FEATURETTES -->

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-6">
          <h2 class="featurette-heading">JUDY BRINGS GOOD LUCK </h2>
          <p class="lead">Порода: Шотландская вислоухая (Scottish fold)<br>Титул: Чемпион (Champion)<br>Окрас: Лиловый (c)<br>Дата рождения: 18.03. 2010<br>Цена: 300$ </p>
        </div>
        <div class="col-md-5">
          <img class="featurette-image img-responsive center-block" src="http://la-luna.com.ua/img/structure/Dgudi_resize(265-211-1).jpg" alt="Generic placeholder image">
        </div>
      </div>

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7 col-md-push-5">
          <h2 class="featurette-heading">ANFISA IRBIS OF SUVEREN </h2>
          <p class="lead">Порода: Британская короткошерстная (British shorthair)<br>Окрас: Черное пятно на серебре ( ns 24)<br>Дата рождения: 23.02.2010<br>Цена: 400$</p>
        </div>
        <div class="col-md-5 col-md-pull-7">
          <img class="featurette-image img-responsive center-block" src="http://la-luna.com.ua/img/structure/anfisa_resize(265-211-1).jpg" alt="Generic placeholder image">
        </div>
      </div>

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-6">
          <h2 class="featurette-heading">LEILA LA LUNA </h2>
          <p class="lead">Порода: Шотландская вислоухая (Scottish fold)<br>Титул: Чемпион (Champion)<br>Окрас: Черный серебристый мраморный (SFS ns 22)<br>Дата рождения: 14.12.2011<br>Цена: 250$</p>
        </div>
        <div class="col-md-5">
          <img class="featurette-image img-responsive center-block" src="http://la-luna.com.ua/img/structure/23_resize(265-211-1).jpg" alt="Generic placeholder image">
        </div>
      </div>

      <hr class="featurette-divider">
