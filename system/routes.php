<?php
$routes = [
    'home' => [
        'namespace'  => 'controller\HomeController',
        'controller' => 'HomeController',
        'action'     => 'index'
    ],
    'page' => [
        'namespace'  => 'controller\PageController',
        'controller' => 'PageController',
        'action'     => 'getPage',
        'slug'       => [
        'reg' => '/^[a-z0-9]{5,50}$/'
        ]
    ],
    
    'gallery' => [
        'namespace'  => 'controller\GalleryController',
        'controller' => 'GalleryController',
        'action'     => 'index'
    ],
    
    'login' => [
        'namespace'  => 'controller\LoginController',
        'controller' => 'LoginController',
        'action'     => 'index'
        
    ],
    
    'registration' => [
        'namespace'  => 'controller\RegistrationController',
        'controller' => 'RegistrationController',
        'action'     => 'index'
    ],
    'logout' => [
        'namespace'  => 'controller\LoginController',
        'controller' => 'LoginController',
        'action'     => 'logout'
    ],
    'admin' => [
        'namespace'  => 'controller\AdminController',
        'controller' => 'AdminController',
        'action'     => 'index'
    ],
    
    'error' => [
        'namespace'     => 'controller\ErrorController',
        'controller'    => 'ErrorController',
        'action'        => 'index',
        'slug'          => [
        'reg' => '/^[a-z0-9]{5,50}$/'
        ]
    ]
];