<?php

namespace controller;

  use engine\Controller;
  use model\PageModel;


class HeaderController extends Controller {
    
    public function index($data)
    {
        $PageModel = new PageModel();
        
        $links = $PageModel->getList();
        
        $data['links'] = $links;
        
        if(isset($_SESSION['user'])){
            
             $data['user'] = $_SESSION['user'];
            
        }else{
            
             $data['username'] = 'Гость';
             $data['user'] = false;
        }
        
        $this->renderView($data, 'header');
    }
}