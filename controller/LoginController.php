<?php

  namespace controller;
  use engine\Controller; 
  use model\LoginModel;
  
    class LoginController extends Controller {

        public function index (){
        
            $data['error'] = '';
           
            if(isset($_SESSION['user'])){
                header('location:http://jun.ua');
            }
        
            if($_SERVER['REQUEST_METHOD'] == 'POST')
            {
                $Model = new LoginModel();
                
                $result = $Model->getUser($_POST);
                
                if($this->verifyPassword($result['password']))
                    
                {
                    $_SESSION['user'] = $result;
                    header('location:http://jun.ua');
                }
                
                else {
                    $data['error'] = 'Невалидный логин или пароль';
                }
                
            }
           
           
            $data['title'] = 'Вход';
                       
            $this->loadHeader(['title'=>$data['title']]);
            $this->renderView($data, 'login');
            $this->loadFooter([]);    
        }
        
        public function logout() {
            
            $this->Sessionlogout();
            
            header('location:/login');
        }
    }