<?php

namespace controller;

use engine\Controller;

class ErrorController extends Controller
{
    public function index($error)
    {
  
        $data = [];
    
        $this->loadHeader(['title'=>'Error-' . $error]);
        $this->renderView($data, $error);
        $this->loadFooter([]);
        }     
    }

