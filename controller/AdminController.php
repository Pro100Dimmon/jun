<?php

namespace controller;

use engine\Controller;

class AdminController extends Controller
{
    public function index()
    {
        if($this->verify_role('ADMIN')){
       
        $data = [];
    
        $this->loadHeader(['title'=>'Админ панель']);
        $this->renderView($data, 'admin');
        $this->loadFooter([]);
        }     
    }
}


