<?php

  namespace controller;

  use engine\Controller; 
  use model\RegistrationModel;
  
    class RegistrationController extends Controller {

        public function index()
        {
            if(isset($_SESSION['user'])){
                header('location:http://jun.ua');
            }
            
            if($_SERVER['REQUEST_METHOD'] == 'POST')
            {
                $Model = new RegistrationModel();
                
                $Model->addUser($_POST);
                
                header('location:http://jun.ua/login');
            }
           
            //$ModelPage = $this->loadModel('Registration');
           
            $data['title'] = 'Регистрация';
                       
            $this->loadHeader(['title'=>$data['title']]);
            $this->renderView($data, 'registration');
            $this->loadFooter([]);    
        }
    }