<?php

namespace controller;

use engine\Controller;

class GalleryController extends Controller
{
    public function index()
    {
    
    
        $this->loadHeader(['title'=>'Gallery']);
        $this->renderView($data, 'Gallery');
        $this->loadFooter([]);
        
    }
}


