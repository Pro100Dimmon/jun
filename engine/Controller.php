<?php

  namespace engine;
  
  use controller\HeaderController;
  use controller\FooterController;

    abstract class Controller 
    {
            public function loadHeader($data)
            {
                $Header = new HeaderController();

                $Header->index($data);

            }

            public function loadFooter($data){
                
                $Footer = new FooterController();

                $Footer->index($data);

            }
            
            public function Sessionlogout() {
                
              //Удаляем все переменные сессии.
                $_SESSION = array();

        // Если требуется уничтожить сессию, также необходимо удалить сессионные cookie.
        // Замечание: Это уничтожит сессию, а не только данные сессии!
        if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        
        setcookie(session_name(), '', time() - 42000,
           $params["path"], $params["domain"],
           $params["secure"], $params["httponly"]
           );
        }

        // Наконец, уничтожаем сессию.
        session_destroy();
                
         }
            
         public function verifyPassword($pass){
             
             return password_verify($_POST['password'],$pass);
         }
         
         public function verify_role($role){
             
             if(isset($_SESSION['user'])&& $_SESSION['user']['role'] === $role){
                 
                 return true;
         }
         header('location:/error/403');
       }

         public function renderView($data, $path) {
                 include './view/' . $path. '.php';
            }
    }