<?php

namespace engine;

use PDO;

final class DB {
    
    private $db;
    
    private static $instance = NULL;

    public function __construct() {
        
        include './system/settings.php';
        
        try {
           $this->db = new PDO("mysql:host={$settings['host']};dbname={$settings['dbname']};charset={$settings['charset']}", $settings['dbuser'], $settings['dbpass']); 
        } catch (PDOException $ex) {
            echo 'Сервис временно не доступен';  
        }
        
    }  
    
    public static function getInstance(){
        
        if (!self::$instance)
        {
            self::$instance = new self();
        }
        
        return self::$instance;
    }
    
    public function query($sql, $type = true){
   
        $dataQuery = $this->db->query($sql);
        
        if($type){
            
            $result = $dataQuery->fetchAll(PDO::FETCH_ASSOC);
            
        }else{
        
            $result = $dataQuery->fetch(PDO::FETCH_ASSOC);
            
        }   
     
        return $result;
    }
    
    public function insertPrepare($params, $name) {
    
        $strParams = '';
        
        $strValues = '';
        
        $values = [];
        
        $j = 0;
        
        foreach ($params as $key => $param)
        {
           $strValues .= ($j == 0) ? '?' : ',?';
           
           $strParams .= ($j == 0) ? $key : ','.$key;
        
           $values[] = $param;

           
           $j++;

        }
        
        $stmt = $this->db->prepare("INSERT INTO {$name}({$strParams}) VALUES({$strValues})");
        
        return $stmt->execute($values);
                
    }
 }