<?php

namespace model;
use engine\Model;

 class RegistrationModel extends Model{
     
     public function addUser($data) {
         
         $db = $this->getDB(); 
         
        $result = $db->insertPrepare(
                [
                   'login'    => $data['login'], 
                   'email'    => $data['email'],
                   'password' => password_hash($data['password'], PASSWORD_DEFAULT),
                   'role'    => 'USER'
                ],
                'user'
           );
         
           return $result;
     }
     
 }